package com.giftshop.model;

public enum Type {

    FURNITURE,

    ELECTRONICS,

    STATIONARY,

    FOODS,

    HAMPERS


}
