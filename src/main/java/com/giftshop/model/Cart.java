package com.giftshop.model;

import java.util.ArrayList;

public class Cart {

    private ArrayList<Item> items;

    private Customer customer;


    public ArrayList<Item> getItems() {
        return items;
    }

    public void setItems(ArrayList<Item> items) {
        this.items = items;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
}
