package com.giftshop.model;

public class ApiPaths {

    public static final String API_VERSION = "v1";
    public static final String ROOT = "/" + API_VERSION;
    public static final String ID = "/{id}";
    public static final String CARTS = ROOT + "/carts";
    public static final String CUSTOMERS = ROOT + "/customers";
    public static final String ITEMS = ROOT + "/items";
    public static final String CREATE = "/create";
}
