package com.giftshop.model;

public enum SubType {

    FOR_HIM,

    FOR_HER,

    MOM,

    DAD,

    BROTHER,

    SISTER,

    INFANT,

    FRIEND
}
