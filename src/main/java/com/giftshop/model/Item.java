package com.giftshop.model;

public class Item {

    private String itemCode;

    private String name;

    private Type giftType;

    private SubType giftSubType;

    private double price;

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Type getGiftType() {
        return giftType;
    }

    public void setGiftType(Type giftType) {
        this.giftType = giftType;
    }

    public SubType getGiftSubType() {
        return giftSubType;
    }

    public void setGiftSubType(SubType giftSubType) {
        this.giftSubType = giftSubType;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}