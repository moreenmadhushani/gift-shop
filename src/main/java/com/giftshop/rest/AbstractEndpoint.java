package com.giftshop.rest;

import com.giftshop.model.ApiParameters;
import com.giftshop.model.ApiPaths;
import com.giftshop.model.Customer;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

public abstract class AbstractEndpoint<T> {

    @GET
    @Path(ApiPaths.ID)
    public Response get(@PathParam(ApiParameters.ID) Long id) {

        return Response.ok().build();
    }

    @POST
    @Path(ApiPaths.CREATE)
    public Response create(T t) {
        return Response.ok().build();
    }

    @PUT
    @Path(ApiPaths.ID)
    public Response edit(T t) {
        return Response.ok().build();
    }


    @DELETE
    @Path(ApiPaths.ID)
    public Response delete(T t) {
        return Response.ok().build();
    }

}
