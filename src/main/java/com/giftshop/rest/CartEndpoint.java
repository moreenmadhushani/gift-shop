package com.giftshop.rest;

import com.giftshop.model.ApiPaths;
import com.giftshop.model.Cart;

import javax.ws.rs.Path;


@Path(ApiPaths.CARTS)
public class CartEndpoint extends AbstractEndpoint<Cart> {

}
