package com.giftshop.rest;

import com.giftshop.model.ApiPaths;
import com.giftshop.model.Item;

import javax.ws.rs.Path;

@Path(ApiPaths.ITEMS)
public class ItemEndpoint extends AbstractEndpoint<Item> {
}
