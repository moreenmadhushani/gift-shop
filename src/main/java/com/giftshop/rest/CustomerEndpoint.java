package com.giftshop.rest;


import com.giftshop.model.ApiPaths;
import com.giftshop.model.Customer;

import javax.ws.rs.Path;

@Path(ApiPaths.CUSTOMERS)
public class CustomerEndpoint extends AbstractEndpoint<Customer> {
}
